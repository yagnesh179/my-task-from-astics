import React, { Component } from 'react'
import {connect} from 'react-redux';

import { signOut, addItem } from '../actions';
import {CSVLink} from 'react-csv';

const mapStateToProps = (state) => ({});
var listItems=[];
var list=[];
var requiredNewData=true;
var sortType=true;
var selectedPage=1;

class Dashboard extends Component {

	constructor(props){
		super(props);
		
		this.state={
		  item_name:'',
		  desc:'',
		  price:''
		}
		
		this.getInputData = this.getInputData.bind(this);
		this.appendItem = this.appendItem.bind(this);
		this.signOut = this.signOut.bind(this);
		this.validateForm=this.validateForm.bind(this);
		this.deleteItem=this.deleteItem.bind(this);
		this.search=this.search.bind(this);
		this.sortData=this.sortData.bind(this);
		this.asc=this.asc.bind(this);
		this.desc=this.desc.bind(this);
		this.generatePages=this.generatePages.bind(this);
	  }

	  getInputData=(e,input)=>{
		input==="item_name"?this.setState({item_name:e.target.value}):
		input==="desc"?this.setState({desc:e.target.value}):
		this.setState({price:e.target.value})
	  }

	  validateForm=()=>{
		var isValidate=false;
	
		if(this.state.item_name!=="" &&
		this.state.price!==""
		){isValidate=true}
		
		return isValidate;
	  }

	appendItem = () => {
		if(this.validateForm())
		this.props.dispatch(addItem(this.state));
  	}

	deleteItem = (index) => {
		if(!!localStorage.getItem('itemList')){
			var	listItems=JSON.parse(localStorage.getItem('itemList'));
			listItems.splice(index, 1);
			localStorage.setItem('itemList',JSON.stringify(listItems));
			this.forceUpdate();
		}
  	}

	search=(e)=>{
		listItems=list.filter((item)=>{
			return item.item_name.includes(e.target.value.toLowerCase())
		});
		requiredNewData=false;
		this.forceUpdate();
	}

	sortData=(col)=>{
		sortType?this.asc(col):this.desc(col)
		requiredNewData=false;
		this.forceUpdate();
	}
	asc=(col)=>{
		if(col==="name"){
			listItems.sort((a,b)=>{
				if ( a.item_name < b.item_name ){
					return -1;
				}
				if ( a.item_name > b.item_name ){
					return 1;
				}
				return 0;
			})
		}else{
			listItems.sort((a,b)=>{
				if ( a.price < b.price ){
					return -1;
				}
				if ( a.price > b.price ){
					return 1;
				}
				return 0;
			})
		}
		sortType=false;
	}
	desc=(col)=>{
		if(col==="name"){
			listItems.sort((a,b)=>{
				if ( a.item_name > b.item_name ){
					return -1;
				}
				if ( a.item_name < b.item_name ){
					return 1;
				}
				return 0;
			})
		}else{
			listItems.sort((a,b)=>{
				if ( a.price > b.price ){
					return -1;
				}
				if ( a.price < b.price ){
					return 1;
				}
				return 0;
			})
		}
		sortType=true;
	}

	generatePages=()=>{
		let pagesDivs=[];
		console.log("ceil",Math.ceil((listItems.length)/5))
			for (let i = 0; i < Math.ceil((listItems.length)/5); i++) {
				pagesDivs.push(<button className="pgButton" onClick={()=>this.changePage(i+1)}>{i+1}</button>);
			} 
			return pagesDivs;
	}

	changePage=(page)=>{
		page==="first"?selectedPage=1:
		page==="last"?selectedPage=Math.ceil((listItems.length)/5):
		page==="next"?selectedPage===Math.ceil((listItems.length)/5)?selectedPage=selectedPage:selectedPage++:
		page==="pre"?selectedPage===1?selectedPage=selectedPage:selectedPage--:
		selectedPage=Number(page)
		this.forceUpdate();
	}

	signOut = () => {
		  this.props.dispatch(signOut());
		  this.props.history.push('/signin');
	}
  
	render(){
		if(requiredNewData){
			if(!!localStorage.getItem('itemList')){
				listItems=JSON.parse(localStorage.getItem('itemList'));
				list=JSON.parse(localStorage.getItem('itemList'));
			}
		}
		const headers=[
			{label:"Item Name",key:"item_name"},
			{label:"Description",key:"desc"},
			{label:"Item Price",key:"price"},
		]
		const csvReport={
			filename:"Report.csv",
			headers:headers,
			data:listItems
		}
		
	  return (
		<div>
			<div style={{display:"flex",justifyContent:"right",textAlign:"right",width:"90%",marginTop:"20px"}}>
			<button className="Button" style={{backgroundColor:"orange"}} onClick={()=>this.signOut()}>SIGN OUT</button>
			</div>
			<form action="/dashboard" style={{display:'flex',flexDirection:'row'}}>
				
				<div className="ItemCard">
					<h3>Add Item</h3>
					<div className="SignupBody">
						<input placeholder="Item name" style={{width:"20%"}} value={this.state.item_name} onChange={(e) => this.getInputData(e,"item_name")} required />
						<input placeholder="Item description" style={{width:"40%"}} value={this.state.desc} onChange={(e) => this.getInputData(e,"desc")} />
						<input placeholder="Item price" type="number" style={{width:"20%"}} value={this.state.price} onChange={(e) => this.getInputData(e,"price")} required/>
						
						<button style={{width:"10%"}} className="Button" onClick={()=>this.appendItem()}>Add</button><br /><br />
						<input placeholder="Search here..." style={{width:"60%"}} onChange={(e) => this.search(e)} /><br /><br />
						<CSVLink {...csvReport}>Export CSV</CSVLink>
					</div>
				</div>

				
			</form>

			<div className="ItemCard">
					<h3>Items</h3>
					<table className="table table-bordered">
						<thead>
							<tr>
							<th scope="col">#</th>
							<th scope="col" style={{color:"blue"}} onClick={()=>{this.sortData("name");}}>Item Name</th>
							<th scope="col">Description</th>
							<th scope="col" style={{color:"blue"}} onClick={()=>this.sortData("price")}>Price</th>
							<th scope="col">Delete</th>
							</tr>
						</thead>
						<tbody>
							{listItems.map((item,index)=>{
								if(index>=(selectedPage*5)-5 && index<(selectedPage*5)){
								return (
								<tr key={index}>
									<th scope="row">{index+1}</th>
									<td>{item.item_name}</td>
									<td>{item.desc}</td>
									<td>{item.price}</td>
									<td>{<button className="pgButton" style={{backgroundColor:"red"}} onClick={()=>{this.deleteItem(index)}} >Delete</button>}</td>
								</tr>
								);
								}
							})}
						</tbody>
					</table>

					<div style={{display:'flex',flexDirection:'row'}}>
						<button className="pgButton" onClick={()=>this.changePage("first")}>First</button><button className="pgButton" onClick={()=>this.changePage("pre")}>Pre</button>
						{
							this.generatePages()
						}
						<button className="pgButton" onClick={()=>this.changePage("next")}>Next</button><button className="pgButton" onClick={()=>this.changePage("last")}>Last</button>
					</div>
			</div>
		</div>
	  );
  
	}
}

export default connect(mapStateToProps,null)(Dashboard);