import React, { Component } from 'react';
import {connect} from 'react-redux';

import { signIn } from '../actions';

const mapStateToProps = (state) => ({
 
});

class SignIn extends Component {

    constructor(props){
      super(props);
      
      this.state={
        username:'',
        password:''
      }
      
      this.validateForm = this.validateForm.bind(this);
      this.signIn = this.signIn.bind(this);

      if(!!localStorage.getItem('loginData')){
        var users=JSON.parse(localStorage.getItem('loginData'));
      console.log(users);
      }
    }

  getInputData=(e,input)=>{
    input==="username"?this.setState({username:e.target.value}):this.setState({password:e.target.value})
  }

  validateForm=()=>{
    var isValidate=false;

    if(
    this.state.username!=="" &&
    this.state.password!==""
    ){isValidate=true}
    
    return isValidate;
  }

  signIn = () => {
      if(this.validateForm()){
        this.props.dispatch(signIn(this.state));
        console.log("a",localStorage.getItem("loginStatus"));
        this.setState({
          username:'',
          password:''
        })

        if(localStorage.getItem("loginStatus")==="1"){
          this.props.history.push('/dashboard');
        }
      }
  }

  render(){
    
    return (
      <form className="Page">
        <div className="Card" style={{marginBottom:"200px"}}>
            <h3>Sign In</h3>
              <div className="SignupBody">
                <label>Username*</label><br/>
                <input placeholder="Username" value={this.state.username} onChange={(e) => this.getInputData(e,"username")} required/><br/><br/>
                <label>Password* </label><br/>
                <input type="password" placeholder="Password" value={this.state.password} onChange={(e) => {this.getInputData(e,"password")}} required/><br/><br/>
                <div className="Center">
                  <button className="Button" onClick={()=>this.signIn()}>SIGN IN</button><br /><br />
                  <span>Not Registered? <a href="signup">Sign Up</a></span>
                </div>
              </div>
            </div>
      </form>
    );

  }

}

export default connect(mapStateToProps,null)(SignIn);
