import React, { Component } from 'react';
import {Checkbox} from '@material-ui/core';
import {connect} from 'react-redux';

import validator from 'validator';
import { signUp } from '../actions';
import {ITEMLIST} from '../constants';


const mapStateToProps = (state) => ({
 
});

class SignUp extends Component {

    constructor(props){
      super(props);
      
      this.state={
        isValidEmail:true,
        isValidPassword:true,
        first_name:'',
        last_name:'',
        username:'',
        email:'',
        password:'',
        agreeTandC:false
      }
      
      this.validateEmail = this.validateEmail.bind(this);
      this.getInputData = this.getInputData.bind(this);
      this.validateForm = this.validateForm.bind(this);
      this.signUp = this.signUp.bind(this);
    }

  validateEmail = (e) => {
    var email = e.target.value
  
    if (validator.isEmail(email)) {
      this.setState({isValidEmail:true});
    } else {
      this.setState({isValidEmail:false});
    }
  }

  validatePassword = (e) => {
    var password = e.target.value
  
    if (password.length>=6 && password.length<=16) {
      this.setState({isValidPassword:true});
    } else {
      this.setState({isValidPassword:false});
    }
  }

  getInputData=(e,input)=>{
    input==="first_name"?this.setState({first_name:e.target.value}):
    input==="last_name"?this.setState({last_name:e.target.value}):
    input==="username"?this.setState({username:e.target.value}):
    input==="email"?this.setState({email:e.target.value}):
    input==="password"?this.setState({password:e.target.value}):this.setState({agreeTandC:e.target.checked})
  }

  validateForm=()=>{
    var isValidate=false;

    if(this.state.first_name!=="" &&
    this.state.last_name!=="" &&
    this.state.username!=="" &&
    this.state.email!=="" &&
    this.state.password!=="" &&
    this.state.isValidEmail &&
    this.state.isValidPassword &&
    this.state.agreeTandC
    ){isValidate=true}
    
    return isValidate;
  }

  signUp = () => {
      console.log(this.state);

      if(this.validateForm()){
        this.props.dispatch(signUp(this.state));
       
      }
  }

  render(){
    
    return (
      <form action="signin" className="Page">
        <div className="Card">
            <h3>Sign Up</h3>
              <div className="SignupBody">
                <label>First name*</label><br/>
                <input placeholder="first name" value={this.state.first_name} onChange={(e) => this.getInputData(e,"first_name")} required /><br/><br/>
                <label>Last name*</label><br/>
                <input placeholder="Last name" value={this.state.last_name} onChange={(e) => this.getInputData(e,"last_name")} required/><br/><br/>
                <label>Username*</label><br/>
                <input placeholder="Username" value={this.state.username} onChange={(e) => this.getInputData(e,"username")} required/><br/><br/>
                <label>Email* { this.state.isValidEmail ? null : <span>Invalid email.</span>}</label><br/>
                <input placeholder="Email" value={this.state.email} onChange={(e) => {this.validateEmail(e);this.getInputData(e,"email")}} required/><br/><br/>
                <label>Password* { this.state.isValidPassword ? null : <span>Password length must be 6-16 characters.</span> }</label><br/>
                <input type="password" placeholder="Password" value={this.state.password} onChange={(e) => {this.validatePassword(e);this.getInputData(e,"password")}} required/><br/><br/>
                <Checkbox onChange={(e) => this.getInputData(e,"tandc")} checked={this.state.agreeTandC} required/>I agree to the <span>Terms and Conditions.</span><br/><br/>
                <div className="Center">
                  <button className="Button" onClick={()=>this.signUp()}>SIGN UP</button><br /><br />
                  <span>Already Registered? <a href="signin">Sign In</a></span>
                </div>
                
              </div>
            </div>
      </form>
    );

  }

}

export default connect(mapStateToProps,null)(SignUp);
