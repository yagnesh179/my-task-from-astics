import './App.css';
import SignUp from './components/signup';
import SignIn from './components/signin';
import Dashboard from './components/dashboard';
import { BrowserRouter, Switch } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute';
import PublicRoute from './components/PublicRoute';
import '../node_modules/bootstrap/dist/css/bootstrap.css';

function App() {
  return (
    <div>
     <BrowserRouter>
        <Switch>
          <PublicRoute restricted={true} component={SignUp} path="/signup" exact />
          <PublicRoute restricted={true} component={SignIn} path="/signin" exact />
          <PublicRoute restricted={true} component={SignIn} path="/" exact />
          <PrivateRoute component={Dashboard} path="/dashboard" exact />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
