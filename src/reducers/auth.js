const initialState=0;

const auth=(state=initialState,action)=>{
    switch(action.type){
        case "SIGNUP":{
          return signUp(action.payload);
        };

        case "SIGNIN":{
          return signIn(action.payload);
        };

        case "SIGNOUT":{
          return signOut();
        };
        default: return state;
    }
}

const signUp=(data)=>{

    var user={
      first_name:data.first_name,
      last_name:data.last_name,
      username:data.username,
      email:data.email,
      password:data.password,
      agreeTandC:data.agreeTandC,
    };

    if(!!localStorage.getItem('loginData')){
      var loginData=JSON.parse(localStorage.getItem('loginData'));
      loginData.push(user);
      localStorage.setItem('loginData', JSON.stringify(loginData));
    }else{
      var loginData=[];
      loginData.push(user);
      localStorage.setItem('loginData', JSON.stringify(loginData));
    }

      return 0;

}

const signIn=(data)=>{
  
  var result=false;

  if(!!localStorage.getItem('loginData')){
    var loginData=JSON.parse(localStorage.getItem('loginData'));
    for(let i=0;i<loginData.length;i++){
      if((loginData[i].username===data.username || loginData[i].email===data.username) && loginData[i].password===data.password){
        result=true;
        break;
      }
    }
  }
  else
    result=false;

  result?localStorage.setItem('loginStatus', '1'):localStorage.setItem('loginStatus', '0');
  return result;
}

const signOut=()=>{
  localStorage.setItem('loginStatus', '0');
  return 0;
}

export default auth;