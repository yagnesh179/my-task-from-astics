import { combineReducers } from "redux";
import auth from "./auth";
import addItem from "./addItem";

const rootReducer=combineReducers({
    auth,
    addItem
});

export default rootReducer;